//
//  CarnivalStreamViewController.h
//  Carnival
//
//  Created by Carnival Mobile
//  Copyright (c) 2015 Carnival Mobile. All rights reserved.
//
//  For documentation see http://docs.carnival.io
//

#import <UIKit/UIKit.h>

@interface CarnivalStreamViewController : UIViewController

@end
